/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9870399892975396, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.946906887755102, 500, 1500, "getDashboardData"], "isController": false}, {"data": [0.004073319755600814, 500, 1500, "getChildCheckInCheckOutByArea"], "isController": false}, {"data": [0.9989711934156379, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9976525821596244, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeedV2"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (health_check_type)"], "isController": false}, {"data": [0.9994845892176064, 500, 1500, "getAllEvents"], "isController": false}, {"data": [0.9993414936473505, 500, 1500, "addOrUpdateUserDevice"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (Relation_Child)"], "isController": false}, {"data": [0.9963963963963964, 500, 1500, "me"], "isController": false}, {"data": [0.9994882292732856, 500, 1500, "getNotificationCount"], "isController": false}, {"data": [0.9989711934156379, 500, 1500, "dismissChildCheckInByVPS"], "isController": false}, {"data": [0.9979423868312757, 500, 1500, "addClassesToArea"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (non_parent_relationship)"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (guardian_rejection_reason)"], "isController": false}, {"data": [0.2186046511627907, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9991042916114963, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.9927536231884058, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.9993416466578886, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}, {"data": [0.999484695455014, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.9998450933312679, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkout_decline_reason)"], "isController": false}, {"data": [0.9976525821596244, 500, 1500, "getAllCentreClasses"], "isController": false}, {"data": [0.9979423868312757, 500, 1500, "getAllArea"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkin_decline_reason)"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 89699, 0, 0.0, 83.56775437853203, 2, 25869, 16.0, 54.0, 169.0, 1688.4100000000944, 294.93055738222375, 1262.7720443372782, 302.2065445999076], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllConfigByCategory", 555, 0, 0.0, 5.185585585585585, 2, 53, 4.0, 9.0, 13.0, 26.43999999999994, 1.860879944743601, 1.7081510523192265, 1.6252542731084407], "isController": false}, {"data": ["getDashboardData", 3136, 0, 0.0, 361.6361607142851, 101, 1734, 350.0, 505.3000000000002, 559.0, 875.5200000000004, 10.481843948874271, 90.98936607572597, 27.903815043585215], "isController": false}, {"data": ["getChildCheckInCheckOutByArea", 491, 0, 0.0, 2826.00814663951, 1452, 5976, 2839.0, 3412.8, 3708.999999999999, 4654.839999999997, 1.630232681682958, 8.780930531668348, 5.412881950900446], "isController": false}, {"data": ["getAllClassInfo", 486, 0, 0.0, 34.04526748971191, 9, 780, 25.0, 55.0, 86.64999999999998, 144.33999999999992, 1.6326091849383404, 1.8032040900051398, 1.5959392520735145], "isController": false}, {"data": ["findAllLevels", 213, 0, 0.0, 19.73239436619718, 5, 935, 9.0, 32.0, 60.29999999999981, 122.87999999999988, 0.7256844601316453, 0.462056902349446, 0.6108789107748811], "isController": false}, {"data": ["getHomefeedV2", 69, 0, 0.0, 21560.202898550724, 5375, 25869, 21900.0, 24410.0, 25425.0, 25869.0, 0.22756280238644122, 2.56519280072919, 0.5802407002255839], "isController": false}, {"data": ["findAllConfigByCategory (health_check_type)", 213, 0, 0.0, 15.915492957746482, 5, 214, 9.0, 31.19999999999999, 46.29999999999998, 110.07999999999959, 0.7256745707277187, 0.7150445721330745, 0.6973279078086673], "isController": false}, {"data": ["getAllEvents", 9701, 0, 0.0, 17.97721884341822, 6, 1400, 11.0, 29.0, 46.0, 122.0, 32.44807171288089, 16.06559800627989, 35.23657787570659], "isController": false}, {"data": ["addOrUpdateUserDevice", 12908, 0, 0.0, 27.28827083978937, 10, 1189, 18.0, 47.0, 68.0, 157.8199999999997, 43.16219596198731, 20.822387505099346, 53.19277400830274], "isController": false}, {"data": ["findAllConfigByCategory (Relation_Child)", 213, 0, 0.0, 16.755868544600947, 5, 107, 10.0, 36.599999999999994, 56.29999999999998, 104.61999999999975, 0.7256696261268321, 0.5796853068083483, 0.6951971711234592], "isController": false}, {"data": ["me", 555, 0, 0.0, 60.457657657657634, 27, 1712, 42.0, 88.40000000000003, 122.79999999999973, 372.7999999999944, 1.8594709051435312, 3.000373045880351, 4.244116701064087], "isController": false}, {"data": ["getNotificationCount", 9770, 0, 0.0, 20.517809621289636, 7, 1150, 13.0, 33.0, 51.44999999999891, 128.5799999999981, 32.678973405269446, 16.97774790195639, 35.224355148443486], "isController": false}, {"data": ["dismissChildCheckInByVPS", 486, 0, 0.0, 21.740740740740733, 7, 675, 12.0, 38.0, 67.64999999999998, 199.39, 1.63256531101041, 0.9071578730126205, 1.2467442121192782], "isController": false}, {"data": ["addClassesToArea", 486, 0, 0.0, 74.1954732510288, 14, 1118, 55.0, 132.3, 168.5999999999999, 404.78, 1.6325159556600604, 0.8943764171145449, 1.187719127897212], "isController": false}, {"data": ["findAllConfigByCategory (non_parent_relationship)", 213, 0, 0.0, 23.31924882629108, 5, 283, 11.0, 49.79999999999998, 86.0, 244.71999999999997, 0.725664681593328, 0.5931458383726714, 0.7015703464622995], "isController": false}, {"data": ["findAllConfigByCategory (guardian_rejection_reason)", 213, 0, 0.0, 17.178403755868526, 5, 464, 10.0, 31.399999999999977, 41.89999999999995, 111.43999999999994, 0.7256745707277187, 0.6909499086518807, 0.7029972403924776], "isController": false}, {"data": ["getCountCheckInOutChildren", 215, 0, 0.0, 1551.2232558139544, 459, 3240, 1555.0, 2003.0, 2133.6, 3156.1600000000003, 0.724147106274482, 0.45683499087237833, 0.8443668407145816], "isController": false}, {"data": ["getCentreHolidaysOfYear", 12839, 0, 0.0, 28.366695225484765, 11, 1517, 18.0, 47.0, 70.0, 162.0, 42.93046662096869, 77.65076807090432, 42.65048267621587], "isController": false}, {"data": ["findAllChildrenByParent", 69, 0, 0.0, 42.5072463768116, 20, 508, 28.0, 62.0, 101.0, 508.0, 0.231647787931486, 0.49768079438405194, 0.35493689381298976], "isController": false}, {"data": ["findAllSchoolConfig", 12911, 0, 0.0, 29.95701339942671, 8, 1160, 20.0, 46.0, 72.0, 177.0, 43.165010631611324, 941.3681810792423, 25.84044227440423], "isController": false}, {"data": ["getChildCheckInCheckOut", 218, 0, 0.0, 5188.376146788985, 2213, 8513, 5218.5, 5798.6, 6049.049999999999, 8186.770000000003, 0.7216894153322586, 49.206673133870076, 2.12066743235817], "isController": false}, {"data": ["getClassAttendanceSummaries", 9703, 0, 0.0, 19.82108626198076, 6, 1155, 11.0, 35.0, 59.0, 144.95999999999913, 32.44651324545388, 18.37790789293286, 35.58343200648897], "isController": false}, {"data": ["getLatestMobileVersion", 12911, 0, 0.0, 14.77972271706294, 8, 772, 12.0, 19.0, 25.0, 63.8799999999992, 43.05604188551515, 23.390444319735547, 25.273342405123973], "isController": false}, {"data": ["findAllConfigByCategory (checkout_decline_reason)", 213, 0, 0.0, 14.877934272300472, 5, 143, 10.0, 29.599999999999994, 44.29999999999998, 85.05999999999969, 0.7256795153959894, 0.5010306810400044, 0.701584687736357], "isController": false}, {"data": ["getAllCentreClasses", 213, 0, 0.0, 35.5117370892019, 14, 782, 24.0, 61.599999999999994, 75.59999999999997, 241.57999999999848, 0.725654792728462, 0.9956493982260636, 0.8234481144047586], "isController": false}, {"data": ["getAllArea", 486, 0, 0.0, 43.5617283950617, 14, 888, 27.0, 81.30000000000001, 110.29999999999995, 253.54999999999984, 1.6326914549094964, 3.0278135477276695, 1.6055862256776006], "isController": false}, {"data": ["findAllConfigByCategory (checkin_decline_reason)", 213, 0, 0.0, 31.02816901408453, 5, 382, 11.0, 69.19999999999999, 145.7999999999994, 338.59999999999985, 0.7256498483970975, 0.48116821002112226, 0.7008473633444623], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 89699, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
